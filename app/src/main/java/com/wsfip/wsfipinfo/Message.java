package com.wsfip.wsfipinfo;


import android.content.Context;
import android.widget.Toast;


/**
 * Just popup message
 * displayed on screen when constructor calls
 * */

public class Message {
    Message (Context context, int textResource) {
        Toast.makeText(context,
                       textResource,
                       Toast.LENGTH_SHORT)
             .show();
    }
}
