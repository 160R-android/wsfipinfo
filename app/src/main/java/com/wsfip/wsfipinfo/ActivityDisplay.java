package com.wsfip.wsfipinfo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.wsfip.wsfipinfo.ActivityLogin.TIMETABLE;


public class ActivityDisplay extends ActionBarActivity {
    private static final String LESSONS = "zajecia";

    private static final DateTimeFormatter TIMESTAMP    = DateTimeFormat.forPattern("ddMMyyyy");
    private static final DateTimeFormatter TIMESTAMP_PL = DateTimeFormat.forPattern("dd-MM-yyyy, EEE")
                                                                        .withLocale(new Locale("pl", "PL"));

    private static byte expandedGroupsCount = 0;

    ActionBar actionBar;
    ExpandableListView expandableListLayout;
    MenuItem buttonExpandCollapse;

    SharedPreferences prefs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_display);

        
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        
        
        actionBar = getSupportActionBar();        
        actionBar.setTitle("WSFIPinfo"); // TODO obtain and display name of university group


        
        expandableListLayout = (ExpandableListView) findViewById(R.id.rootList);
        
        expandableListLayout.setOnGroupCollapseListener(
                new ExpandableListView.OnGroupCollapseListener() {

                    @Override
                    public void onGroupCollapse(int a) {
                        ActivityDisplay.expandedGroupsCount--;
                        setIconExpand(false);
                    }
                });
        
        expandableListLayout.setOnGroupExpandListener(
                new ExpandableListView.OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        ActivityDisplay.expandedGroupsCount++;
                        setIconExpand(true);
                    }
                });


        try { populateExpandableListLayout();}
        catch (JSONException e) { new Message(this, R.string.error);}
    }




    private void populateExpandableListLayout() throws JSONException {

        expandableListLayout.destroyDrawingCache();

        expandableListLayout.setAdapter(


                new BaseExpandableListAdapter() {

                    private static final String DATE_OF_LESSON = "data";
                    private static final String NAME_OF_LESSON = "nazwa";
                    private static final String TIME_STRT      = "godzina_od";
                    private static final String TIME_ENDS      = "godzina_do";
                    private static final String LECTOR         = "wykladowca";
                    private static final String TYPE           = "typ";
                    private static final String LAB            = "sala";

                    private JSONArray timetable = new JSONArray(prefs.getString(TIMETABLE, "{}"));
                    private boolean scrollScheduled = false;



                            //     METHODS TO WORK WITH EXPANDABLE GROUPS


                    @Override
                    public int getGroupCount() {
                        return timetable.length();
                    }



                    @Override
                    public JSONObject getGroup(int group) {
                        try { return timetable.getJSONObject(group); }
                        catch (JSONException e) {return new JSONObject();}
                    }



                    @Override
                    public TextView getGroupView(final int group, boolean b, View cached, ViewGroup p) {

                        final DateTime lesson;

                        try { lesson = TIMESTAMP.parseDateTime(getGroup(group).getString(DATE_OF_LESSON));}
                        catch (JSONException e) { return new TextView(ActivityDisplay.this){{
                                                                    setText(getString(R.string.error));}};}

                        if (!scrollScheduled && (lesson.isAfterNow() || lesson.isEqualNow())) {
                            scrollScheduled = true;

                            expandableListLayout.postDelayed(
                                    new Runnable() {
                                        @Override public void run() {
                                            expandableListLayout.smoothScrollToPosition(group);
                                            expandableListLayout.expandGroup           (group);
                                            expandableListLayout.setSelectedGroup      (group);}},
                                    1000);
                        }


                        TextView groupView = (cached == null) ? new TextView(ActivityDisplay.this)
                                                              : (TextView) cached;
                        groupView.setText(lesson.toString(TIMESTAMP_PL));


                        return groupView;
                    }


                            //      METHODS TO WORK WITH EXPANDABLE GROUPS CHILDES


                    @Override
                    public int getChildrenCount(int group) {
                        try { return timetable.getJSONObject(group).getJSONArray(LESSONS).length();}
                        catch (JSONException e) { return 0; }
                    }


                    @Override
                    public JSONObject getChild(int group, int child) {
                        try { return getGroup(group).getJSONArray(LESSONS).getJSONObject(child);}
                        catch (JSONException e) { return new JSONObject(); }
                    }


                    @Override
                    public View getChildView(int group, int child, boolean b, View cached, ViewGroup p) {

                        View childLayout = (cached == null) ? getLayoutInflater().inflate(R.layout.expand_list_group_child, null)
                                                            : cached;

                        try {JSONObject lection = getChild(group, child);

                            ((TextView)childLayout.findViewById(R.id.time_start))
                                    .setText(lection.getString(TIME_STRT));
                            ((TextView)childLayout.findViewById(R.id.time_end))
                                    .setText(lection.getString(TIME_ENDS));
                            ((TextView)childLayout.findViewById(R.id.lab))
                                    .setText(lection.getString(LAB));
                            ((TextView)childLayout.findViewById(R.id.type))
                                    .setText(lection.getString(TYPE).equals("null") ? "(cw)" : "wyk");
                            ((TextView)childLayout.findViewById(R.id.name))
                                    .setText(lection.getString(NAME_OF_LESSON));
                            ((TextView)childLayout.findViewById(R.id.lector))
                                    .setText(lection.getString(LECTOR));


                        } catch (JSONException e) {
                            childLayout.destroyDrawingCache();
                            childLayout.setBackgroundColor(getResources().getColor(R.color.red));
                        }

                        return childLayout;
                    }


                            //      USELESS INHERITED METHODS

                    @Override public long getGroupId(int a) { return a;}
                    @Override public long getChildId(int a, int child) {return child;}
                    @Override public boolean hasStableIds() {return true;}
                    @Override public boolean isChildSelectable(int a, int b) {return false;}
                });
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_display, menu);

        buttonExpandCollapse = menu.findItem(R.id.button_expand_collapse);

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.button_expand_collapse:

                if (expandedGroupsCount == 0) {
                    for (int i = 0; i < expandableListLayout.getChildCount(); i++) expandableListLayout.expandGroup(i);
                    setIconExpand(false);
                } else {
                    for (int i = 0; i < expandableListLayout.getChildCount(); i++) expandableListLayout.collapseGroup(i);
                    setIconExpand(true);
                }

                break;

            case R.id.Logout:

                prefs.edit().clear().apply();

                startActivity(new Intent(this, ActivityLogin.class));

                finish();

                break;

        }

        return true;
    }



    private void setIconExpand (boolean set_unset) {
        buttonExpandCollapse.setIcon(set_unset  ? R.drawable.ic_action_collapse
                                                : R.drawable.ic_action_expand);
    }

}