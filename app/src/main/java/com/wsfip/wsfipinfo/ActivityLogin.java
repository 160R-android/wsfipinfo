package com.wsfip.wsfipinfo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.joda.time.DateTime;




public class ActivityLogin extends Activity {
    static final String TIMETABLE = "TIMETABLE";
    static final String LAST_SYNC_DATE = "last_sync_date";

    private static final String REMEMBER = "zapamietaj";
    private static final String LOGIN    = "nr_albumu";
    private static final String PASSW    = "haslo";


    EditText edtxtLogin, edtxtPassw;

    SharedPreferences prefs;




    /**
     * Called when instance of activity create
     * */
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);


        edtxtLogin = (EditText)findViewById(R.id.edtxtLogin);
        edtxtPassw = (EditText)findViewById(R.id.edtxtPassw);


        //check, if login and password is saved in SharedPreferences try to get TIMETABLE
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        if (prefs.getBoolean(REMEMBER, false)) {

            String login = prefs.getString(LOGIN, "");
            String passw = prefs.getString(PASSW, "");

            edtxtLogin.setText(login);
            edtxtPassw.setText(passw);


            auth(login, passw);
        }
    }




    /**
     * Called on click "Login" button on login screen
     * */
    public void onClickButtonLogin( View loginButton ) {

        String login = edtxtLogin.getText().toString();
        String passw = edtxtPassw.getText().toString();

        //check length of password and index num
        if ((passw.length() >= 4) && (login.length() >= 4)) {

            if (((CheckBox)findViewById(R.id.SaveNumAndPasswd)).isChecked()) {

                SharedPreferences.Editor prefsEdit = prefs.edit();

                prefsEdit.putBoolean(REMEMBER, true);
                prefsEdit.putString(LOGIN, login);
                prefsEdit.putString(PASSW, passw);

                prefsEdit.apply();
            }

            auth(login, passw);

        } else

            new Message(this, R.string.missing_login_or_passwd);
    }




    /**
     * Used to obtain timetable from server
     * */
    private void auth( final String login, final String passw ) {

        new AsyncHttpClient().get(

                getString(R.string.API_URI),

                new RequestParams() {{
                    put(LOGIN, login);
                    put(PASSW, passw);
                }},

                new TextHttpResponseHandler() {


                    @Override public void onStart() { lockUI(true); }


                    @Override public void onSuccess( int statusCode, Header[] h, String response ) {

                            // save date and time of this sync in prefs

                        SharedPreferences.Editor prefsEdit = prefs.edit();

                        prefsEdit.putString(TIMETABLE, response);
                        prefsEdit.putString(LAST_SYNC_DATE, DateTime.now().toString());

                        prefsEdit.apply();

                            // start new activity

                        startActivityDisplay();
                    }


                    @Override public void onFailure( int statusCode, Header[] h, String r,Throwable e ) {

                            // if query failure, but TIMETABLE is saved in preferences, try with it

                        if (prefs.contains(TIMETABLE)) {
                            new Message(ActivityLogin.this, R.string.error_no_connection_display_old_data);
                            startActivityDisplay();
                        } else {
                            lockUI(false);
                            new Message(ActivityLogin.this, R.string.error_no_connection);
                        }
                    }


                    private void startActivityDisplay(){
                        startActivity(new Intent(ActivityLogin.this,
                                                 ActivityDisplay.class));
                        finish();
                    }


                    private void lockUI( boolean block_unblock ){

                        Button buttonLogin = (Button) findViewById(R.id.buttonLogin);
                        buttonLogin.setClickable(!block_unblock);
                        buttonLogin.setText(block_unblock ? R.string.loading
                                                          : R.string.login);

                        findViewById(R.id.progress).setVisibility(block_unblock ? View.VISIBLE
                                                                                : View.INVISIBLE);
                    }
                });
    }
}
